import 'package:freezed_annotation/freezed_annotation.dart';

class FreezedKey extends JsonKey {
  const FreezedKey(String name) : super(name: name);
}