import 'package:freezed_annotation/freezed_annotation.dart';
import '../../core/freezed_key/freezed_key.dart';
import '../../../domain/entities/pagination_data_request/pagination_data_request.dart';

part 'pagination_request_model.freezed.dart';
part 'pagination_request_model.g.dart';

@Freezed()
class PaginationRequestModel with _$PaginationRequestModel {
  const PaginationRequestModel._();
  factory PaginationRequestModel({
    required int page,
    @FreezedKey('per_page') required int perPage,
    @FreezedKey('sort') String? sort,
    @FreezedKey('order') String? order,
  }) = _PaginationRequestModel;

  factory PaginationRequestModel.fromJson(Map<String, dynamic> json) =>
      _$PaginationRequestModelFromJson(json);

  factory PaginationRequestModel.fromEntity(PaginationDataRequest entity) =>
      PaginationRequestModel(
        page: entity.page,
        perPage: entity.perPage,
        sort: entity.sortBy?.name,
        order: entity.order?.name,
      );
}
