// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagination_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PaginationRequestModelImpl _$$PaginationRequestModelImplFromJson(
        Map<String, dynamic> json) =>
    _$PaginationRequestModelImpl(
      page: json['page'] as int,
      perPage: json['per_page'] as int,
      sort: json['sort'] as String?,
      order: json['order'] as String?,
    );

Map<String, dynamic> _$$PaginationRequestModelImplToJson(
        _$PaginationRequestModelImpl instance) =>
    <String, dynamic>{
      'page': instance.page,
      'per_page': instance.perPage,
      'sort': instance.sort,
      'order': instance.order,
    };
