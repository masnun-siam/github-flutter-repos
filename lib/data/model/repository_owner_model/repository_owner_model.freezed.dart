// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'repository_owner_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

RepositoryOwnerModel _$RepositoryOwnerModelFromJson(Map<String, dynamic> json) {
  return _RepositoryOwnerModel.fromJson(json);
}

/// @nodoc
mixin _$RepositoryOwnerModel {
  @FreezedKey('login')
  String? get login => throw _privateConstructorUsedError;
  @FreezedKey('id')
  int? get id => throw _privateConstructorUsedError;
  @FreezedKey('avatar_url')
  String? get avatarUrl => throw _privateConstructorUsedError;
  @FreezedKey('gravatar_id')
  String? get gravatarId => throw _privateConstructorUsedError;
  @FreezedKey('url')
  String? get url => throw _privateConstructorUsedError;
  @FreezedKey('type')
  String? get type => throw _privateConstructorUsedError;
  @FreezedKey('site_admin')
  bool? get siteAdmin => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RepositoryOwnerModelCopyWith<RepositoryOwnerModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepositoryOwnerModelCopyWith<$Res> {
  factory $RepositoryOwnerModelCopyWith(RepositoryOwnerModel value,
          $Res Function(RepositoryOwnerModel) then) =
      _$RepositoryOwnerModelCopyWithImpl<$Res, RepositoryOwnerModel>;
  @useResult
  $Res call(
      {@FreezedKey('login') String? login,
      @FreezedKey('id') int? id,
      @FreezedKey('avatar_url') String? avatarUrl,
      @FreezedKey('gravatar_id') String? gravatarId,
      @FreezedKey('url') String? url,
      @FreezedKey('type') String? type,
      @FreezedKey('site_admin') bool? siteAdmin});
}

/// @nodoc
class _$RepositoryOwnerModelCopyWithImpl<$Res,
        $Val extends RepositoryOwnerModel>
    implements $RepositoryOwnerModelCopyWith<$Res> {
  _$RepositoryOwnerModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? login = freezed,
    Object? id = freezed,
    Object? avatarUrl = freezed,
    Object? gravatarId = freezed,
    Object? url = freezed,
    Object? type = freezed,
    Object? siteAdmin = freezed,
  }) {
    return _then(_value.copyWith(
      login: freezed == login
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as String?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      avatarUrl: freezed == avatarUrl
          ? _value.avatarUrl
          : avatarUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      gravatarId: freezed == gravatarId
          ? _value.gravatarId
          : gravatarId // ignore: cast_nullable_to_non_nullable
              as String?,
      url: freezed == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      siteAdmin: freezed == siteAdmin
          ? _value.siteAdmin
          : siteAdmin // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RepositoryOwnerModelImplCopyWith<$Res>
    implements $RepositoryOwnerModelCopyWith<$Res> {
  factory _$$RepositoryOwnerModelImplCopyWith(_$RepositoryOwnerModelImpl value,
          $Res Function(_$RepositoryOwnerModelImpl) then) =
      __$$RepositoryOwnerModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@FreezedKey('login') String? login,
      @FreezedKey('id') int? id,
      @FreezedKey('avatar_url') String? avatarUrl,
      @FreezedKey('gravatar_id') String? gravatarId,
      @FreezedKey('url') String? url,
      @FreezedKey('type') String? type,
      @FreezedKey('site_admin') bool? siteAdmin});
}

/// @nodoc
class __$$RepositoryOwnerModelImplCopyWithImpl<$Res>
    extends _$RepositoryOwnerModelCopyWithImpl<$Res, _$RepositoryOwnerModelImpl>
    implements _$$RepositoryOwnerModelImplCopyWith<$Res> {
  __$$RepositoryOwnerModelImplCopyWithImpl(_$RepositoryOwnerModelImpl _value,
      $Res Function(_$RepositoryOwnerModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? login = freezed,
    Object? id = freezed,
    Object? avatarUrl = freezed,
    Object? gravatarId = freezed,
    Object? url = freezed,
    Object? type = freezed,
    Object? siteAdmin = freezed,
  }) {
    return _then(_$RepositoryOwnerModelImpl(
      login: freezed == login
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as String?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      avatarUrl: freezed == avatarUrl
          ? _value.avatarUrl
          : avatarUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      gravatarId: freezed == gravatarId
          ? _value.gravatarId
          : gravatarId // ignore: cast_nullable_to_non_nullable
              as String?,
      url: freezed == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      siteAdmin: freezed == siteAdmin
          ? _value.siteAdmin
          : siteAdmin // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RepositoryOwnerModelImpl extends _RepositoryOwnerModel {
  _$RepositoryOwnerModelImpl(
      {@FreezedKey('login') this.login,
      @FreezedKey('id') this.id,
      @FreezedKey('avatar_url') this.avatarUrl,
      @FreezedKey('gravatar_id') this.gravatarId,
      @FreezedKey('url') this.url,
      @FreezedKey('type') this.type,
      @FreezedKey('site_admin') this.siteAdmin})
      : super._();

  factory _$RepositoryOwnerModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$RepositoryOwnerModelImplFromJson(json);

  @override
  @FreezedKey('login')
  final String? login;
  @override
  @FreezedKey('id')
  final int? id;
  @override
  @FreezedKey('avatar_url')
  final String? avatarUrl;
  @override
  @FreezedKey('gravatar_id')
  final String? gravatarId;
  @override
  @FreezedKey('url')
  final String? url;
  @override
  @FreezedKey('type')
  final String? type;
  @override
  @FreezedKey('site_admin')
  final bool? siteAdmin;

  @override
  String toString() {
    return 'RepositoryOwnerModel(login: $login, id: $id, avatarUrl: $avatarUrl, gravatarId: $gravatarId, url: $url, type: $type, siteAdmin: $siteAdmin)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RepositoryOwnerModelImpl &&
            (identical(other.login, login) || other.login == login) &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.avatarUrl, avatarUrl) ||
                other.avatarUrl == avatarUrl) &&
            (identical(other.gravatarId, gravatarId) ||
                other.gravatarId == gravatarId) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.siteAdmin, siteAdmin) ||
                other.siteAdmin == siteAdmin));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, login, id, avatarUrl, gravatarId, url, type, siteAdmin);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RepositoryOwnerModelImplCopyWith<_$RepositoryOwnerModelImpl>
      get copyWith =>
          __$$RepositoryOwnerModelImplCopyWithImpl<_$RepositoryOwnerModelImpl>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RepositoryOwnerModelImplToJson(
      this,
    );
  }
}

abstract class _RepositoryOwnerModel extends RepositoryOwnerModel {
  factory _RepositoryOwnerModel(
          {@FreezedKey('login') final String? login,
          @FreezedKey('id') final int? id,
          @FreezedKey('avatar_url') final String? avatarUrl,
          @FreezedKey('gravatar_id') final String? gravatarId,
          @FreezedKey('url') final String? url,
          @FreezedKey('type') final String? type,
          @FreezedKey('site_admin') final bool? siteAdmin}) =
      _$RepositoryOwnerModelImpl;
  _RepositoryOwnerModel._() : super._();

  factory _RepositoryOwnerModel.fromJson(Map<String, dynamic> json) =
      _$RepositoryOwnerModelImpl.fromJson;

  @override
  @FreezedKey('login')
  String? get login;
  @override
  @FreezedKey('id')
  int? get id;
  @override
  @FreezedKey('avatar_url')
  String? get avatarUrl;
  @override
  @FreezedKey('gravatar_id')
  String? get gravatarId;
  @override
  @FreezedKey('url')
  String? get url;
  @override
  @FreezedKey('type')
  String? get type;
  @override
  @FreezedKey('site_admin')
  bool? get siteAdmin;
  @override
  @JsonKey(ignore: true)
  _$$RepositoryOwnerModelImplCopyWith<_$RepositoryOwnerModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
