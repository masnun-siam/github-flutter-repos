// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repository_owner_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RepositoryOwnerModelImpl _$$RepositoryOwnerModelImplFromJson(
        Map<String, dynamic> json) =>
    _$RepositoryOwnerModelImpl(
      login: json['login'] as String?,
      id: json['id'] as int?,
      avatarUrl: json['avatar_url'] as String?,
      gravatarId: json['gravatar_id'] as String?,
      url: json['url'] as String?,
      type: json['type'] as String?,
      siteAdmin: json['site_admin'] as bool?,
    );

Map<String, dynamic> _$$RepositoryOwnerModelImplToJson(
        _$RepositoryOwnerModelImpl instance) =>
    <String, dynamic>{
      'login': instance.login,
      'id': instance.id,
      'avatar_url': instance.avatarUrl,
      'gravatar_id': instance.gravatarId,
      'url': instance.url,
      'type': instance.type,
      'site_admin': instance.siteAdmin,
    };
