import 'package:freezed_annotation/freezed_annotation.dart';

import '../../core/freezed_key/freezed_key.dart';

part 'repository_owner_model.freezed.dart';
part 'repository_owner_model.g.dart';

@freezed
class RepositoryOwnerModel with _$RepositoryOwnerModel {
  factory RepositoryOwnerModel({
    @FreezedKey('login') String? login,
    @FreezedKey('id') int? id,
    @FreezedKey('avatar_url') String? avatarUrl,
    @FreezedKey('gravatar_id') String? gravatarId,
    @FreezedKey('url') String? url,
    @FreezedKey('type') String? type,
    @FreezedKey('site_admin') bool? siteAdmin,
  }) = _RepositoryOwnerModel;
  factory RepositoryOwnerModel.fromJson(Map<String, Object?> json) =>
      _$RepositoryOwnerModelFromJson(json);

  RepositoryOwnerModel._();
}
