import 'package:freezed_annotation/freezed_annotation.dart';
import '../../core/freezed_key/freezed_key.dart';
import '../repository_model/repository_model.dart';
import '../../../domain/entities/paginated_response/paginated_response.dart';

part 'paginated_response_model.freezed.dart';
part 'paginated_response_model.g.dart';

@freezed
class PaginatedResponseModel with _$PaginatedResponseModel {
  const PaginatedResponseModel._();
  factory PaginatedResponseModel({
    @FreezedKey('total_count') int? totalCount,
    @FreezedKey('incomplete_results') bool? incompleteResults,
    @FreezedKey('items') List<RepositoryModel>? items,
  }) = _PaginatedResponseModel;

  factory PaginatedResponseModel.fromJson(Map<String, dynamic> json) =>
      _$PaginatedResponseModelFromJson(json);

  bool canFetchMore(int currentPage, int perPage) {
    if (items?.isEmpty ?? true) return false;
    if (totalCount == null) return false;
    return (currentPage * perPage) < totalCount!;
  }

  PaginatedResponse toEntity(int currentPage, int perPage) {
    return PaginatedResponse(
      canFetchMore: canFetchMore(currentPage, perPage),
      data: items?.map((e) => e.toEntity()).toList() ?? [],
      currentPage: currentPage,
      perPage: perPage,
    );
  }
}
