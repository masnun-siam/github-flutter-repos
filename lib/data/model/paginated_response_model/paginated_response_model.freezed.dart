// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'paginated_response_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

PaginatedResponseModel _$PaginatedResponseModelFromJson(
    Map<String, dynamic> json) {
  return _PaginatedResponseModel.fromJson(json);
}

/// @nodoc
mixin _$PaginatedResponseModel {
  @FreezedKey('total_count')
  int? get totalCount => throw _privateConstructorUsedError;
  @FreezedKey('incomplete_results')
  bool? get incompleteResults => throw _privateConstructorUsedError;
  @FreezedKey('items')
  List<RepositoryModel>? get items => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginatedResponseModelCopyWith<PaginatedResponseModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginatedResponseModelCopyWith<$Res> {
  factory $PaginatedResponseModelCopyWith(PaginatedResponseModel value,
          $Res Function(PaginatedResponseModel) then) =
      _$PaginatedResponseModelCopyWithImpl<$Res, PaginatedResponseModel>;
  @useResult
  $Res call(
      {@FreezedKey('total_count') int? totalCount,
      @FreezedKey('incomplete_results') bool? incompleteResults,
      @FreezedKey('items') List<RepositoryModel>? items});
}

/// @nodoc
class _$PaginatedResponseModelCopyWithImpl<$Res,
        $Val extends PaginatedResponseModel>
    implements $PaginatedResponseModelCopyWith<$Res> {
  _$PaginatedResponseModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? totalCount = freezed,
    Object? incompleteResults = freezed,
    Object? items = freezed,
  }) {
    return _then(_value.copyWith(
      totalCount: freezed == totalCount
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int?,
      incompleteResults: freezed == incompleteResults
          ? _value.incompleteResults
          : incompleteResults // ignore: cast_nullable_to_non_nullable
              as bool?,
      items: freezed == items
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<RepositoryModel>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PaginatedResponseModelImplCopyWith<$Res>
    implements $PaginatedResponseModelCopyWith<$Res> {
  factory _$$PaginatedResponseModelImplCopyWith(
          _$PaginatedResponseModelImpl value,
          $Res Function(_$PaginatedResponseModelImpl) then) =
      __$$PaginatedResponseModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@FreezedKey('total_count') int? totalCount,
      @FreezedKey('incomplete_results') bool? incompleteResults,
      @FreezedKey('items') List<RepositoryModel>? items});
}

/// @nodoc
class __$$PaginatedResponseModelImplCopyWithImpl<$Res>
    extends _$PaginatedResponseModelCopyWithImpl<$Res,
        _$PaginatedResponseModelImpl>
    implements _$$PaginatedResponseModelImplCopyWith<$Res> {
  __$$PaginatedResponseModelImplCopyWithImpl(
      _$PaginatedResponseModelImpl _value,
      $Res Function(_$PaginatedResponseModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? totalCount = freezed,
    Object? incompleteResults = freezed,
    Object? items = freezed,
  }) {
    return _then(_$PaginatedResponseModelImpl(
      totalCount: freezed == totalCount
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int?,
      incompleteResults: freezed == incompleteResults
          ? _value.incompleteResults
          : incompleteResults // ignore: cast_nullable_to_non_nullable
              as bool?,
      items: freezed == items
          ? _value._items
          : items // ignore: cast_nullable_to_non_nullable
              as List<RepositoryModel>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$PaginatedResponseModelImpl extends _PaginatedResponseModel {
  _$PaginatedResponseModelImpl(
      {@FreezedKey('total_count') this.totalCount,
      @FreezedKey('incomplete_results') this.incompleteResults,
      @FreezedKey('items') final List<RepositoryModel>? items})
      : _items = items,
        super._();

  factory _$PaginatedResponseModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$PaginatedResponseModelImplFromJson(json);

  @override
  @FreezedKey('total_count')
  final int? totalCount;
  @override
  @FreezedKey('incomplete_results')
  final bool? incompleteResults;
  final List<RepositoryModel>? _items;
  @override
  @FreezedKey('items')
  List<RepositoryModel>? get items {
    final value = _items;
    if (value == null) return null;
    if (_items is EqualUnmodifiableListView) return _items;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'PaginatedResponseModel(totalCount: $totalCount, incompleteResults: $incompleteResults, items: $items)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PaginatedResponseModelImpl &&
            (identical(other.totalCount, totalCount) ||
                other.totalCount == totalCount) &&
            (identical(other.incompleteResults, incompleteResults) ||
                other.incompleteResults == incompleteResults) &&
            const DeepCollectionEquality().equals(other._items, _items));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, totalCount, incompleteResults,
      const DeepCollectionEquality().hash(_items));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PaginatedResponseModelImplCopyWith<_$PaginatedResponseModelImpl>
      get copyWith => __$$PaginatedResponseModelImplCopyWithImpl<
          _$PaginatedResponseModelImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$PaginatedResponseModelImplToJson(
      this,
    );
  }
}

abstract class _PaginatedResponseModel extends PaginatedResponseModel {
  factory _PaginatedResponseModel(
          {@FreezedKey('total_count') final int? totalCount,
          @FreezedKey('incomplete_results') final bool? incompleteResults,
          @FreezedKey('items') final List<RepositoryModel>? items}) =
      _$PaginatedResponseModelImpl;
  _PaginatedResponseModel._() : super._();

  factory _PaginatedResponseModel.fromJson(Map<String, dynamic> json) =
      _$PaginatedResponseModelImpl.fromJson;

  @override
  @FreezedKey('total_count')
  int? get totalCount;
  @override
  @FreezedKey('incomplete_results')
  bool? get incompleteResults;
  @override
  @FreezedKey('items')
  List<RepositoryModel>? get items;
  @override
  @JsonKey(ignore: true)
  _$$PaginatedResponseModelImplCopyWith<_$PaginatedResponseModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
