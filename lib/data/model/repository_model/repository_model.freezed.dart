// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'repository_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

RepositoryModel _$RepositoryModelFromJson(Map<String, dynamic> json) {
  return _RepositoryModel.fromJson(json);
}

/// @nodoc
mixin _$RepositoryModel {
  @FreezedKey('id')
  int? get id => throw _privateConstructorUsedError;
  @FreezedKey('name')
  String? get name => throw _privateConstructorUsedError;
  @FreezedKey('full_name')
  String? get fullName => throw _privateConstructorUsedError;
  @FreezedKey('owner')
  RepositoryOwnerModel? get owner => throw _privateConstructorUsedError;
  @FreezedKey('description')
  String? get description => throw _privateConstructorUsedError;
  @FreezedKey('created_at')
  String? get createdAt => throw _privateConstructorUsedError;
  @FreezedKey('updated_at')
  String? get updatedAt => throw _privateConstructorUsedError;
  @FreezedKey('homepage')
  String? get homepage => throw _privateConstructorUsedError;
  @FreezedKey('svn_url')
  String? get svnUrl => throw _privateConstructorUsedError;
  @FreezedKey('stargazers_count')
  int? get stargazersCount => throw _privateConstructorUsedError;
  @FreezedKey('language')
  String? get language => throw _privateConstructorUsedError;
  @FreezedKey('forks_count')
  int? get forksCount => throw _privateConstructorUsedError;
  @FreezedKey('open_issues_count')
  int? get openIssuesCount => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RepositoryModelCopyWith<RepositoryModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepositoryModelCopyWith<$Res> {
  factory $RepositoryModelCopyWith(
          RepositoryModel value, $Res Function(RepositoryModel) then) =
      _$RepositoryModelCopyWithImpl<$Res, RepositoryModel>;
  @useResult
  $Res call(
      {@FreezedKey('id') int? id,
      @FreezedKey('name') String? name,
      @FreezedKey('full_name') String? fullName,
      @FreezedKey('owner') RepositoryOwnerModel? owner,
      @FreezedKey('description') String? description,
      @FreezedKey('created_at') String? createdAt,
      @FreezedKey('updated_at') String? updatedAt,
      @FreezedKey('homepage') String? homepage,
      @FreezedKey('svn_url') String? svnUrl,
      @FreezedKey('stargazers_count') int? stargazersCount,
      @FreezedKey('language') String? language,
      @FreezedKey('forks_count') int? forksCount,
      @FreezedKey('open_issues_count') int? openIssuesCount});

  $RepositoryOwnerModelCopyWith<$Res>? get owner;
}

/// @nodoc
class _$RepositoryModelCopyWithImpl<$Res, $Val extends RepositoryModel>
    implements $RepositoryModelCopyWith<$Res> {
  _$RepositoryModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? fullName = freezed,
    Object? owner = freezed,
    Object? description = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? homepage = freezed,
    Object? svnUrl = freezed,
    Object? stargazersCount = freezed,
    Object? language = freezed,
    Object? forksCount = freezed,
    Object? openIssuesCount = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      fullName: freezed == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String?,
      owner: freezed == owner
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as RepositoryOwnerModel?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      homepage: freezed == homepage
          ? _value.homepage
          : homepage // ignore: cast_nullable_to_non_nullable
              as String?,
      svnUrl: freezed == svnUrl
          ? _value.svnUrl
          : svnUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      stargazersCount: freezed == stargazersCount
          ? _value.stargazersCount
          : stargazersCount // ignore: cast_nullable_to_non_nullable
              as int?,
      language: freezed == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String?,
      forksCount: freezed == forksCount
          ? _value.forksCount
          : forksCount // ignore: cast_nullable_to_non_nullable
              as int?,
      openIssuesCount: freezed == openIssuesCount
          ? _value.openIssuesCount
          : openIssuesCount // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $RepositoryOwnerModelCopyWith<$Res>? get owner {
    if (_value.owner == null) {
      return null;
    }

    return $RepositoryOwnerModelCopyWith<$Res>(_value.owner!, (value) {
      return _then(_value.copyWith(owner: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$RepositoryModelImplCopyWith<$Res>
    implements $RepositoryModelCopyWith<$Res> {
  factory _$$RepositoryModelImplCopyWith(_$RepositoryModelImpl value,
          $Res Function(_$RepositoryModelImpl) then) =
      __$$RepositoryModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@FreezedKey('id') int? id,
      @FreezedKey('name') String? name,
      @FreezedKey('full_name') String? fullName,
      @FreezedKey('owner') RepositoryOwnerModel? owner,
      @FreezedKey('description') String? description,
      @FreezedKey('created_at') String? createdAt,
      @FreezedKey('updated_at') String? updatedAt,
      @FreezedKey('homepage') String? homepage,
      @FreezedKey('svn_url') String? svnUrl,
      @FreezedKey('stargazers_count') int? stargazersCount,
      @FreezedKey('language') String? language,
      @FreezedKey('forks_count') int? forksCount,
      @FreezedKey('open_issues_count') int? openIssuesCount});

  @override
  $RepositoryOwnerModelCopyWith<$Res>? get owner;
}

/// @nodoc
class __$$RepositoryModelImplCopyWithImpl<$Res>
    extends _$RepositoryModelCopyWithImpl<$Res, _$RepositoryModelImpl>
    implements _$$RepositoryModelImplCopyWith<$Res> {
  __$$RepositoryModelImplCopyWithImpl(
      _$RepositoryModelImpl _value, $Res Function(_$RepositoryModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? fullName = freezed,
    Object? owner = freezed,
    Object? description = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? homepage = freezed,
    Object? svnUrl = freezed,
    Object? stargazersCount = freezed,
    Object? language = freezed,
    Object? forksCount = freezed,
    Object? openIssuesCount = freezed,
  }) {
    return _then(_$RepositoryModelImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      fullName: freezed == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String?,
      owner: freezed == owner
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as RepositoryOwnerModel?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      homepage: freezed == homepage
          ? _value.homepage
          : homepage // ignore: cast_nullable_to_non_nullable
              as String?,
      svnUrl: freezed == svnUrl
          ? _value.svnUrl
          : svnUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      stargazersCount: freezed == stargazersCount
          ? _value.stargazersCount
          : stargazersCount // ignore: cast_nullable_to_non_nullable
              as int?,
      language: freezed == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String?,
      forksCount: freezed == forksCount
          ? _value.forksCount
          : forksCount // ignore: cast_nullable_to_non_nullable
              as int?,
      openIssuesCount: freezed == openIssuesCount
          ? _value.openIssuesCount
          : openIssuesCount // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RepositoryModelImpl extends _RepositoryModel {
  _$RepositoryModelImpl(
      {@FreezedKey('id') this.id,
      @FreezedKey('name') this.name,
      @FreezedKey('full_name') this.fullName,
      @FreezedKey('owner') this.owner,
      @FreezedKey('description') this.description,
      @FreezedKey('created_at') this.createdAt,
      @FreezedKey('updated_at') this.updatedAt,
      @FreezedKey('homepage') this.homepage,
      @FreezedKey('svn_url') this.svnUrl,
      @FreezedKey('stargazers_count') this.stargazersCount,
      @FreezedKey('language') this.language,
      @FreezedKey('forks_count') this.forksCount,
      @FreezedKey('open_issues_count') this.openIssuesCount})
      : super._();

  factory _$RepositoryModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$RepositoryModelImplFromJson(json);

  @override
  @FreezedKey('id')
  final int? id;
  @override
  @FreezedKey('name')
  final String? name;
  @override
  @FreezedKey('full_name')
  final String? fullName;
  @override
  @FreezedKey('owner')
  final RepositoryOwnerModel? owner;
  @override
  @FreezedKey('description')
  final String? description;
  @override
  @FreezedKey('created_at')
  final String? createdAt;
  @override
  @FreezedKey('updated_at')
  final String? updatedAt;
  @override
  @FreezedKey('homepage')
  final String? homepage;
  @override
  @FreezedKey('svn_url')
  final String? svnUrl;
  @override
  @FreezedKey('stargazers_count')
  final int? stargazersCount;
  @override
  @FreezedKey('language')
  final String? language;
  @override
  @FreezedKey('forks_count')
  final int? forksCount;
  @override
  @FreezedKey('open_issues_count')
  final int? openIssuesCount;

  @override
  String toString() {
    return 'RepositoryModel(id: $id, name: $name, fullName: $fullName, owner: $owner, description: $description, createdAt: $createdAt, updatedAt: $updatedAt, homepage: $homepage, svnUrl: $svnUrl, stargazersCount: $stargazersCount, language: $language, forksCount: $forksCount, openIssuesCount: $openIssuesCount)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RepositoryModelImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.owner, owner) || other.owner == owner) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            (identical(other.homepage, homepage) ||
                other.homepage == homepage) &&
            (identical(other.svnUrl, svnUrl) || other.svnUrl == svnUrl) &&
            (identical(other.stargazersCount, stargazersCount) ||
                other.stargazersCount == stargazersCount) &&
            (identical(other.language, language) ||
                other.language == language) &&
            (identical(other.forksCount, forksCount) ||
                other.forksCount == forksCount) &&
            (identical(other.openIssuesCount, openIssuesCount) ||
                other.openIssuesCount == openIssuesCount));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      name,
      fullName,
      owner,
      description,
      createdAt,
      updatedAt,
      homepage,
      svnUrl,
      stargazersCount,
      language,
      forksCount,
      openIssuesCount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RepositoryModelImplCopyWith<_$RepositoryModelImpl> get copyWith =>
      __$$RepositoryModelImplCopyWithImpl<_$RepositoryModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RepositoryModelImplToJson(
      this,
    );
  }
}

abstract class _RepositoryModel extends RepositoryModel {
  factory _RepositoryModel(
          {@FreezedKey('id') final int? id,
          @FreezedKey('name') final String? name,
          @FreezedKey('full_name') final String? fullName,
          @FreezedKey('owner') final RepositoryOwnerModel? owner,
          @FreezedKey('description') final String? description,
          @FreezedKey('created_at') final String? createdAt,
          @FreezedKey('updated_at') final String? updatedAt,
          @FreezedKey('homepage') final String? homepage,
          @FreezedKey('svn_url') final String? svnUrl,
          @FreezedKey('stargazers_count') final int? stargazersCount,
          @FreezedKey('language') final String? language,
          @FreezedKey('forks_count') final int? forksCount,
          @FreezedKey('open_issues_count') final int? openIssuesCount}) =
      _$RepositoryModelImpl;
  _RepositoryModel._() : super._();

  factory _RepositoryModel.fromJson(Map<String, dynamic> json) =
      _$RepositoryModelImpl.fromJson;

  @override
  @FreezedKey('id')
  int? get id;
  @override
  @FreezedKey('name')
  String? get name;
  @override
  @FreezedKey('full_name')
  String? get fullName;
  @override
  @FreezedKey('owner')
  RepositoryOwnerModel? get owner;
  @override
  @FreezedKey('description')
  String? get description;
  @override
  @FreezedKey('created_at')
  String? get createdAt;
  @override
  @FreezedKey('updated_at')
  String? get updatedAt;
  @override
  @FreezedKey('homepage')
  String? get homepage;
  @override
  @FreezedKey('svn_url')
  String? get svnUrl;
  @override
  @FreezedKey('stargazers_count')
  int? get stargazersCount;
  @override
  @FreezedKey('language')
  String? get language;
  @override
  @FreezedKey('forks_count')
  int? get forksCount;
  @override
  @FreezedKey('open_issues_count')
  int? get openIssuesCount;
  @override
  @JsonKey(ignore: true)
  _$$RepositoryModelImplCopyWith<_$RepositoryModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
