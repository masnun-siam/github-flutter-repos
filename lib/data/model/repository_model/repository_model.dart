import 'package:fpdart/fpdart.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/repository/repository_entity.dart';
import '../../core/freezed_key/freezed_key.dart';
import '../repository_owner_model/repository_owner_model.dart';

part 'repository_model.freezed.dart';
part 'repository_model.g.dart';

@freezed
class RepositoryModel with _$RepositoryModel {
  factory RepositoryModel({
    @FreezedKey('id') int? id,
    @FreezedKey('name') String? name,
    @FreezedKey('full_name') String? fullName,
    @FreezedKey('owner') RepositoryOwnerModel? owner,
    @FreezedKey('description') String? description,
    @FreezedKey('created_at') String? createdAt,
    @FreezedKey('updated_at') String? updatedAt,
    @FreezedKey('homepage') String? homepage,
    @FreezedKey('svn_url') String? svnUrl,
    @FreezedKey('stargazers_count') int? stargazersCount,
    @FreezedKey('language') String? language,
    @FreezedKey('forks_count') int? forksCount,
    @FreezedKey('open_issues_count') int? openIssuesCount,
  }) = _RepositoryModel;
  factory RepositoryModel.fromJson(Map<String, Object?> json) =>
      _$RepositoryModelFromJson(json);

  RepositoryModel._();

  Repository toEntity() {
    return Repository(
      id: id ?? 0,
      name: name ?? '',
      fullName: fullName ?? '',
      description: description ?? '',
      forks: forksCount ?? 0,
      stars: stargazersCount ?? 0,
      language: language ?? '',
      owner: owner?.login ?? '',
      ownerPhoto: owner?.avatarUrl ?? '',
      lastUpdatedAt: DateTime.tryParse(updatedAt ?? '') ?? DateTime.now(),
      createdAt: DateTime.tryParse(createdAt ?? '') ?? DateTime.now(),
      openIssues: openIssuesCount ?? 0,
      homepage: optionOf(homepage),
      url: svnUrl ?? '',
    );
  }
}
