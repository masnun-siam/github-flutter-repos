// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_repository_impl.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$apiRepositoryHash() => r'21239b1193145d930bf6116012c868cc408cb661';

/// See also [apiRepository].
@ProviderFor(apiRepository)
final apiRepositoryProvider = AutoDisposeProvider<ApiRepository>.internal(
  apiRepository,
  name: r'apiRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$apiRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ApiRepositoryRef = AutoDisposeProviderRef<ApiRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
