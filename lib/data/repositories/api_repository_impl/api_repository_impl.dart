import 'package:dio/dio.dart';
import 'package:fpdart/fpdart.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../../domain/entities/paginated_response/paginated_response.dart';
import '../../../domain/entities/pagination_data_request/pagination_data_request.dart';
import '../../../domain/failures/failures.dart';
import '../../../domain/repositories/api_repository/api_repository.dart';
import '../../../global/helpers/dio_helpers/handle_dio_exception.dart';
import '../../model/paginated_response_model/paginated_response_model.dart';
import '../../model/pagination_request_model/pagination_request_model.dart';
import '../../source/remote/http_client.dart';

part 'api_repository_impl.g.dart';

@riverpod
ApiRepository apiRepository(ApiRepositoryRef ref) {
  return ApiRepositoryImpl(ref.read(httpClientProvider));
}

class ApiRepositoryImpl extends ApiRepository {
  final HttpClient client;

  ApiRepositoryImpl(this.client);

  @override
  Future<Either<Failures, PaginatedResponse>> getRepositories(
    PaginationDataRequest request,
  ) async {
    try {
      final response = await client.get(
        '/search/repositories',
        queryParameters: {
          'q': 'Flutter',
          ...PaginationRequestModel.fromEntity(request).toJson(),
        },
        addToken: false,
      );

      return Right(
        PaginatedResponseModel.fromJson(response ?? {})
            .toEntity(request.page, request.perPage),
      );
    } on DioException catch (e) {
      return Left(
        handleDioExceptions(e),
      );
    }
  }
}
