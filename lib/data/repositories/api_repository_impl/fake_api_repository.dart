import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:fpdart/fpdart.dart';
import '../../model/paginated_response_model/paginated_response_model.dart';
import '../../../domain/entities/paginated_response/paginated_response.dart';
import '../../../domain/entities/pagination_data_request/pagination_data_request.dart';
import '../../../domain/failures/failures.dart';
import '../../../domain/repositories/api_repository/api_repository.dart';

class FakeApiRepository extends ApiRepository {
  @override
  Future<Either<Failures, PaginatedResponse>> getRepositories(
    PaginationDataRequest request,
  ) async {
    final jsonStr = await rootBundle.loadString('json/repo_list.json');
    final jsonMap = jsonDecode(jsonStr);
    return Right(
      PaginatedResponseModel.fromJson(jsonMap).toEntity(1, 10),
    );
  }
}
