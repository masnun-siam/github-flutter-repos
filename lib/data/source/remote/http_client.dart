import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../../global/constants/strings.dart';
import '../../../global/global.dart';
import 'cache_interceptor.dart';
import 'token_interceptor.dart';

part 'http_client.g.dart';

@riverpod
HttpClient httpClient(HttpClientRef ref) {
  return HttpClient();
}

class HttpClient {
  final Dio _dio = Dio()
    ..interceptors.addAll([
      PrettyDioLogger(
        request: true,
        responseBody: true,
        requestBody: true,
        requestHeader: true,
        responseHeader: true,
        compact: true,
        error: true,
      ),
      TokenInterceptor(),
      CacheInterceptor(),
    ])
    ..options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.json,
      connectTimeout: const Duration(seconds: 1000),
      receiveTimeout: const Duration(seconds: 1000),
      sendTimeout: const Duration(seconds: 1000),
    );

  HttpClient();

  Future<JsonMap?> get(
    String url, {
    bool addToken = true,
    Map<String, dynamic> queryParameters = const {},
  }) async {
    var queryParams = queryParameters;
    if (addToken) {
      queryParams = {
        ...queryParameters,
        'add-token': true,
      };
    }

    final response = await _dio.get(
      url,
      queryParameters: queryParams,
    );

    return response.data;
  }

  Future<JsonMap?> post(
    String url,
    dynamic data, {
    bool addToken = true,
    Map<String, dynamic> queryParameters = const {},
  }) async {
    var queryParams = queryParameters;
    if (addToken) {
      queryParams = {
        ...queryParameters,
        'add-token': true,
      };
    }
  
    final response = await _dio.post(
      url,
      data: data,
      queryParameters: queryParams,
    );
    return response.data;
  }

  Future<JsonMap?> patch(
    String url,
    dynamic data, {
    bool addToken = true,
    Map<String, dynamic> queryParameters = const {},
  }) async {
    var queryParams = queryParameters;

    if (addToken) {
      queryParams = {
        ...queryParameters,
        'add-token': true,
      };
    }

    _dio.options.contentType = 'application/x-www-form-urlencoded';

    final response = await _dio.patch(
      url,
      data: data,
      queryParameters: queryParams,
    );
    return response.data;
  }

  Future<JsonMap?> put(
    String url,
    dynamic data, {
    bool addToken = true,
    Map<String, dynamic> queryParameters = const {},
  }) async {
    var queryParams = queryParameters;

    if (addToken) {
      queryParams = {
        ...queryParameters,
        'add-token': true,
      };
    }

    _dio.options.contentType = 'application/x-www-form-urlencoded';

    final response = await _dio.put(
      url,
      data: data,
      queryParameters: queryParams,
    );
    return response.data;
  }

  Future<JsonMap?> delete(String url,
      {bool addToken = true,
      Map<String, dynamic> queryParameters = const {},}) async {
    var queryParams = {
      ...queryParameters,
      if (addToken) 'add-token': true,
    };

    final response = await _dio.delete(
      url,
      queryParameters: queryParams,
    );
    return response.data;
  }
}
