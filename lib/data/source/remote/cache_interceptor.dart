import 'dart:convert';

import 'package:dio/dio.dart';
import '../../../global/helpers/cache_helper/cache_timeout_checker.dart';
import '../../../global/helpers/shared_preference_helper/shared_preference_helper.dart';

class CacheInterceptor extends Interceptor {
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    final isCacheValid = cacheTimeoutChecker();

    if (isCacheValid) {
      final url = options.uri.toString();
      final cachedData = Preference.getString(url);
      if (cachedData.isNotEmpty) {
        handler.resolve(
          Response(
            requestOptions: options,
            data: jsonDecode(cachedData),
            statusCode: 200,
          ),
        );
        return;
      } else {
        handler.next(options);
      }
    } else {
      Preference.setString('cacheDateTime', DateTime.now().toIso8601String());
      handler.next(options);
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    if (response.statusCode == 200) {
      final url = response.requestOptions.uri.toString();
      Preference.setString(url, jsonEncode(response.data));
    }
    handler.next(response);
  }
}
