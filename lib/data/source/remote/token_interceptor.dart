import 'package:dio/dio.dart';
import '../../../global/constants/strings.dart';
import '../../../global/helpers/shared_preference_helper/shared_preference_helper.dart';

class TokenInterceptor extends Interceptor {
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    if (options.queryParameters['add-token'] == true) {
      final token = Preference.getString(tokenKey); // get the token from the storage
      if (token.isEmpty) {
        handler.reject(
          DioException.badResponse(
            statusCode: 401,
            requestOptions: options,
            response: Response(
              requestOptions: options,
              data: {
                'message': 'Unauthorized',
                'success': false,
                'data': null,
              },
            ),
          ),
        );
        return;
      } else {
        final bearerToken = 'Bearer $token';
        options.headers.addAll(
          {'Authorization': bearerToken},
        );
      }
    }
    if (options.queryParameters.containsKey('add-token')) {
      options.queryParameters.remove('add-token');
    }
    handler.next(options);
  }
}
