import 'package:flutter/material.dart';
import 'data/repositories/api_repository_impl/api_repository_impl.dart';
import 'data/repositories/api_repository_impl/fake_api_repository.dart';

import '../global/global.dart';
import 'app_root.dart';
import 'global/helpers/shared_preference_helper/shared_preference_helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Preference.load();
  runApp(
    ProviderScope(
      overrides: [
        apiRepositoryProvider.overrideWithValue(FakeApiRepository()),
      ],
      observers: [Logger()],
      child: const AppRoot(),
    ),
  );
}
