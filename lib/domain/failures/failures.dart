import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class Failures with _$Failures {
  const Failures._();
  const factory Failures.network(
          [@Default('Please check your internet connection') String message]) =
      _Network;
  const factory Failures.server([@Default('Server Error') String message]) =
      _Server;
  const factory Failures.unexpected(
      [@Default('Unexpected error') String message]) = _Unexpected;
  const factory Failures.timeout(
          [@Default('Connection timeout, please try again') String message]) =
      _Timeout;
  const factory Failures.response(String message) = _Response;

  @override
  String toString() => message;
}
