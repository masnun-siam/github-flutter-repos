import 'package:fpdart/fpdart.dart';
import '../../entities/paginated_response/paginated_response.dart';
import '../../entities/pagination_data_request/pagination_data_request.dart';
import '../../failures/failures.dart';

abstract class ApiRepository {
  Future<Either<Failures, PaginatedResponse>> getRepositories(
    PaginationDataRequest request,
  );
}