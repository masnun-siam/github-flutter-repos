// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pagination_data_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$PaginationDataRequest {
  int get page => throw _privateConstructorUsedError;
  int get perPage => throw _privateConstructorUsedError;
  SortedBy? get sortBy => throw _privateConstructorUsedError;
  SortingOrder? get order => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PaginationDataRequestCopyWith<PaginationDataRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginationDataRequestCopyWith<$Res> {
  factory $PaginationDataRequestCopyWith(PaginationDataRequest value,
          $Res Function(PaginationDataRequest) then) =
      _$PaginationDataRequestCopyWithImpl<$Res, PaginationDataRequest>;
  @useResult
  $Res call({int page, int perPage, SortedBy? sortBy, SortingOrder? order});
}

/// @nodoc
class _$PaginationDataRequestCopyWithImpl<$Res,
        $Val extends PaginationDataRequest>
    implements $PaginationDataRequestCopyWith<$Res> {
  _$PaginationDataRequestCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? perPage = null,
    Object? sortBy = freezed,
    Object? order = freezed,
  }) {
    return _then(_value.copyWith(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      perPage: null == perPage
          ? _value.perPage
          : perPage // ignore: cast_nullable_to_non_nullable
              as int,
      sortBy: freezed == sortBy
          ? _value.sortBy
          : sortBy // ignore: cast_nullable_to_non_nullable
              as SortedBy?,
      order: freezed == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as SortingOrder?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PaginationDataRequestImplCopyWith<$Res>
    implements $PaginationDataRequestCopyWith<$Res> {
  factory _$$PaginationDataRequestImplCopyWith(
          _$PaginationDataRequestImpl value,
          $Res Function(_$PaginationDataRequestImpl) then) =
      __$$PaginationDataRequestImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int page, int perPage, SortedBy? sortBy, SortingOrder? order});
}

/// @nodoc
class __$$PaginationDataRequestImplCopyWithImpl<$Res>
    extends _$PaginationDataRequestCopyWithImpl<$Res,
        _$PaginationDataRequestImpl>
    implements _$$PaginationDataRequestImplCopyWith<$Res> {
  __$$PaginationDataRequestImplCopyWithImpl(_$PaginationDataRequestImpl _value,
      $Res Function(_$PaginationDataRequestImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? page = null,
    Object? perPage = null,
    Object? sortBy = freezed,
    Object? order = freezed,
  }) {
    return _then(_$PaginationDataRequestImpl(
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      perPage: null == perPage
          ? _value.perPage
          : perPage // ignore: cast_nullable_to_non_nullable
              as int,
      sortBy: freezed == sortBy
          ? _value.sortBy
          : sortBy // ignore: cast_nullable_to_non_nullable
              as SortedBy?,
      order: freezed == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as SortingOrder?,
    ));
  }
}

/// @nodoc

class _$PaginationDataRequestImpl implements _PaginationDataRequest {
  const _$PaginationDataRequestImpl(
      {this.page = 1, this.perPage = 10, this.sortBy, this.order});

  @override
  @JsonKey()
  final int page;
  @override
  @JsonKey()
  final int perPage;
  @override
  final SortedBy? sortBy;
  @override
  final SortingOrder? order;

  @override
  String toString() {
    return 'PaginationDataRequest(page: $page, perPage: $perPage, sortBy: $sortBy, order: $order)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PaginationDataRequestImpl &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.perPage, perPage) || other.perPage == perPage) &&
            (identical(other.sortBy, sortBy) || other.sortBy == sortBy) &&
            (identical(other.order, order) || other.order == order));
  }

  @override
  int get hashCode => Object.hash(runtimeType, page, perPage, sortBy, order);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PaginationDataRequestImplCopyWith<_$PaginationDataRequestImpl>
      get copyWith => __$$PaginationDataRequestImplCopyWithImpl<
          _$PaginationDataRequestImpl>(this, _$identity);
}

abstract class _PaginationDataRequest implements PaginationDataRequest {
  const factory _PaginationDataRequest(
      {final int page,
      final int perPage,
      final SortedBy? sortBy,
      final SortingOrder? order}) = _$PaginationDataRequestImpl;

  @override
  int get page;
  @override
  int get perPage;
  @override
  SortedBy? get sortBy;
  @override
  SortingOrder? get order;
  @override
  @JsonKey(ignore: true)
  _$$PaginationDataRequestImplCopyWith<_$PaginationDataRequestImpl>
      get copyWith => throw _privateConstructorUsedError;
}
