import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../global/enums/enums.dart';
part 'pagination_data_request.freezed.dart';

@freezed
class PaginationDataRequest with _$PaginationDataRequest {
  const factory PaginationDataRequest({
    @Default(1) int page,
    @Default(10) int perPage,
    SortedBy? sortBy,
    SortingOrder? order,
  }) = _PaginationDataRequest;
}