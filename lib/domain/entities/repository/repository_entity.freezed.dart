// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'repository_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Repository {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  int get forks => throw _privateConstructorUsedError;
  int get stars => throw _privateConstructorUsedError;
  int get openIssues => throw _privateConstructorUsedError;
  String get language => throw _privateConstructorUsedError;
  String get owner => throw _privateConstructorUsedError;
  String get ownerPhoto => throw _privateConstructorUsedError;
  DateTime get lastUpdatedAt => throw _privateConstructorUsedError;
  DateTime get createdAt => throw _privateConstructorUsedError;
  Option<String> get homepage => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RepositoryCopyWith<Repository> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RepositoryCopyWith<$Res> {
  factory $RepositoryCopyWith(
          Repository value, $Res Function(Repository) then) =
      _$RepositoryCopyWithImpl<$Res, Repository>;
  @useResult
  $Res call(
      {int id,
      String name,
      String fullName,
      String description,
      int forks,
      int stars,
      int openIssues,
      String language,
      String owner,
      String ownerPhoto,
      DateTime lastUpdatedAt,
      DateTime createdAt,
      Option<String> homepage,
      String url});
}

/// @nodoc
class _$RepositoryCopyWithImpl<$Res, $Val extends Repository>
    implements $RepositoryCopyWith<$Res> {
  _$RepositoryCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? fullName = null,
    Object? description = null,
    Object? forks = null,
    Object? stars = null,
    Object? openIssues = null,
    Object? language = null,
    Object? owner = null,
    Object? ownerPhoto = null,
    Object? lastUpdatedAt = null,
    Object? createdAt = null,
    Object? homepage = null,
    Object? url = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      forks: null == forks
          ? _value.forks
          : forks // ignore: cast_nullable_to_non_nullable
              as int,
      stars: null == stars
          ? _value.stars
          : stars // ignore: cast_nullable_to_non_nullable
              as int,
      openIssues: null == openIssues
          ? _value.openIssues
          : openIssues // ignore: cast_nullable_to_non_nullable
              as int,
      language: null == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      owner: null == owner
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as String,
      ownerPhoto: null == ownerPhoto
          ? _value.ownerPhoto
          : ownerPhoto // ignore: cast_nullable_to_non_nullable
              as String,
      lastUpdatedAt: null == lastUpdatedAt
          ? _value.lastUpdatedAt
          : lastUpdatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      homepage: null == homepage
          ? _value.homepage
          : homepage // ignore: cast_nullable_to_non_nullable
              as Option<String>,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RepositoryImplCopyWith<$Res>
    implements $RepositoryCopyWith<$Res> {
  factory _$$RepositoryImplCopyWith(
          _$RepositoryImpl value, $Res Function(_$RepositoryImpl) then) =
      __$$RepositoryImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String name,
      String fullName,
      String description,
      int forks,
      int stars,
      int openIssues,
      String language,
      String owner,
      String ownerPhoto,
      DateTime lastUpdatedAt,
      DateTime createdAt,
      Option<String> homepage,
      String url});
}

/// @nodoc
class __$$RepositoryImplCopyWithImpl<$Res>
    extends _$RepositoryCopyWithImpl<$Res, _$RepositoryImpl>
    implements _$$RepositoryImplCopyWith<$Res> {
  __$$RepositoryImplCopyWithImpl(
      _$RepositoryImpl _value, $Res Function(_$RepositoryImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? fullName = null,
    Object? description = null,
    Object? forks = null,
    Object? stars = null,
    Object? openIssues = null,
    Object? language = null,
    Object? owner = null,
    Object? ownerPhoto = null,
    Object? lastUpdatedAt = null,
    Object? createdAt = null,
    Object? homepage = null,
    Object? url = null,
  }) {
    return _then(_$RepositoryImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      forks: null == forks
          ? _value.forks
          : forks // ignore: cast_nullable_to_non_nullable
              as int,
      stars: null == stars
          ? _value.stars
          : stars // ignore: cast_nullable_to_non_nullable
              as int,
      openIssues: null == openIssues
          ? _value.openIssues
          : openIssues // ignore: cast_nullable_to_non_nullable
              as int,
      language: null == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      owner: null == owner
          ? _value.owner
          : owner // ignore: cast_nullable_to_non_nullable
              as String,
      ownerPhoto: null == ownerPhoto
          ? _value.ownerPhoto
          : ownerPhoto // ignore: cast_nullable_to_non_nullable
              as String,
      lastUpdatedAt: null == lastUpdatedAt
          ? _value.lastUpdatedAt
          : lastUpdatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      homepage: null == homepage
          ? _value.homepage
          : homepage // ignore: cast_nullable_to_non_nullable
              as Option<String>,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$RepositoryImpl implements _Repository {
  _$RepositoryImpl(
      {required this.id,
      required this.name,
      required this.fullName,
      required this.description,
      required this.forks,
      required this.stars,
      required this.openIssues,
      required this.language,
      required this.owner,
      required this.ownerPhoto,
      required this.lastUpdatedAt,
      required this.createdAt,
      required this.homepage,
      required this.url});

  @override
  final int id;
  @override
  final String name;
  @override
  final String fullName;
  @override
  final String description;
  @override
  final int forks;
  @override
  final int stars;
  @override
  final int openIssues;
  @override
  final String language;
  @override
  final String owner;
  @override
  final String ownerPhoto;
  @override
  final DateTime lastUpdatedAt;
  @override
  final DateTime createdAt;
  @override
  final Option<String> homepage;
  @override
  final String url;

  @override
  String toString() {
    return 'Repository(id: $id, name: $name, fullName: $fullName, description: $description, forks: $forks, stars: $stars, openIssues: $openIssues, language: $language, owner: $owner, ownerPhoto: $ownerPhoto, lastUpdatedAt: $lastUpdatedAt, createdAt: $createdAt, homepage: $homepage, url: $url)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RepositoryImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.forks, forks) || other.forks == forks) &&
            (identical(other.stars, stars) || other.stars == stars) &&
            (identical(other.openIssues, openIssues) ||
                other.openIssues == openIssues) &&
            (identical(other.language, language) ||
                other.language == language) &&
            (identical(other.owner, owner) || other.owner == owner) &&
            (identical(other.ownerPhoto, ownerPhoto) ||
                other.ownerPhoto == ownerPhoto) &&
            (identical(other.lastUpdatedAt, lastUpdatedAt) ||
                other.lastUpdatedAt == lastUpdatedAt) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.homepage, homepage) ||
                other.homepage == homepage) &&
            (identical(other.url, url) || other.url == url));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      name,
      fullName,
      description,
      forks,
      stars,
      openIssues,
      language,
      owner,
      ownerPhoto,
      lastUpdatedAt,
      createdAt,
      homepage,
      url);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RepositoryImplCopyWith<_$RepositoryImpl> get copyWith =>
      __$$RepositoryImplCopyWithImpl<_$RepositoryImpl>(this, _$identity);
}

abstract class _Repository implements Repository {
  factory _Repository(
      {required final int id,
      required final String name,
      required final String fullName,
      required final String description,
      required final int forks,
      required final int stars,
      required final int openIssues,
      required final String language,
      required final String owner,
      required final String ownerPhoto,
      required final DateTime lastUpdatedAt,
      required final DateTime createdAt,
      required final Option<String> homepage,
      required final String url}) = _$RepositoryImpl;

  @override
  int get id;
  @override
  String get name;
  @override
  String get fullName;
  @override
  String get description;
  @override
  int get forks;
  @override
  int get stars;
  @override
  int get openIssues;
  @override
  String get language;
  @override
  String get owner;
  @override
  String get ownerPhoto;
  @override
  DateTime get lastUpdatedAt;
  @override
  DateTime get createdAt;
  @override
  Option<String> get homepage;
  @override
  String get url;
  @override
  @JsonKey(ignore: true)
  _$$RepositoryImplCopyWith<_$RepositoryImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
