import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:fpdart/fpdart.dart';

part 'repository_entity.freezed.dart';

@freezed
class Repository with _$Repository {
  factory Repository({
    required int id,
    required String name,
    required String fullName,
    required String description,
    required int forks,
    required int stars,
    required int openIssues,
    required String language,
    required String owner,
    required String ownerPhoto,
    required DateTime lastUpdatedAt,
    required DateTime createdAt,
    required Option<String> homepage,
    required String url,
  }) = _Repository;
}
