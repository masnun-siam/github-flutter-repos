import 'package:freezed_annotation/freezed_annotation.dart';
import '../pagination_data_request/pagination_data_request.dart';

import '../repository/repository_entity.dart';

part 'paginated_response.freezed.dart';

@freezed
class PaginatedResponse with _$PaginatedResponse {
  const PaginatedResponse._();
  factory PaginatedResponse({
    required int currentPage,
    required int perPage,
    required bool canFetchMore,
    required List<Repository> data,
  }) = _PaginatedResponse;

  PaginationDataRequest nextPage([PaginationDataRequest? request]) {
    return (request ?? const PaginationDataRequest())
        .copyWith(page: currentPage + 1);
  }
}
