import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../global/enums/enums.dart';
import '../controller/home_controller.dart';

class SortBottomSheet extends HookConsumerWidget {
  const SortBottomSheet({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final sort = ref.watch(sortControllerProvider);
    final notifier = ref.read(sortControllerProvider.notifier);
    final selectedSortedBy = useState<SortedBy?>(sort?.$1);
    final selectedSortingOrder =
        useState<SortingOrder?>(sort?.$2 ?? SortingOrder.desc);

    return Padding(
      padding: const EdgeInsets.only(
        left: 16,
        right: 16,
        top: 8,
        bottom: 20,
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Sort by'),
            const Divider(),
            RadioListTile<int>(
              value: 0,
              groupValue: selectedSortedBy.value?.index,
              title: const Text('Stars'),
              onChanged: (value) {
                value != null
                    ? selectedSortedBy.value = SortedBy.values[value]
                    : null;
              },
            ),
            RadioListTile<int>(
              value: 1,
              groupValue: selectedSortedBy.value?.index,
              title: const Text('Updates'),
              onChanged: (value) {
                value != null
                    ? selectedSortedBy.value = SortedBy.values[value]
                    : null;
              },
            ),
            const SizedBox(height: 14),
            const Text('Order'),
            const Divider(),
            RadioListTile<int>(
              value: 0,
              groupValue: selectedSortingOrder.value?.index,
              title: const Text('Ascending'),
              onChanged: (value) {
                value != null
                    ? selectedSortingOrder.value = SortingOrder.values[value]
                    : null;
              },
            ),
            RadioListTile<int>(
              value: 1,
              groupValue: selectedSortingOrder.value?.index,
              title: const Text('Descending'),
              onChanged: (value) {
                value != null
                    ? selectedSortingOrder.value = SortingOrder.values[value]
                    : null;
              },
            ),
            const SizedBox(height: 14),
            Row(
              children: [
                if (sort != null)
                  TextButton(
                    onPressed: () {
                      notifier.clearSorting();
                      Navigator.pop(context);
                    },
                    child: const Text('Clear Sort'),
                  ),
                const Spacer(),
                ElevatedButton(
                  onPressed: selectedSortedBy.value == null
                      ? null
                      : () {
                          notifier.setSorting(
                            selectedSortedBy.value!,
                            selectedSortingOrder.value!,
                          );
                          Navigator.pop(context);
                        },
                  child: const Text('Apply'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
