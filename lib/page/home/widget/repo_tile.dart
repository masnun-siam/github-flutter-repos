import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart';
import '../../../global/constants/strings.dart';
import '../../../global/extensions/date_time_ext.dart';
import '../../../global/global.dart';

import '../../../domain/entities/repository/repository_entity.dart';

class RepositoryTile extends StatelessWidget {
  final Repository repository;

  const RepositoryTile({super.key, required this.repository});

  factory RepositoryTile.loading() {
    return RepositoryTile(
      repository: Repository(
        id: 0,
        fullName: 'Loading/loading',
        name: 'Loading...',
        description: '',
        forks: 0,
        language: '',
        openIssues: 0,
        owner: 'Loading...',
        createdAt: DateTime.now(),
        homepage: none(),
        url: '',
        lastUpdatedAt: DateTime.now(),
        stars: 0,
        ownerPhoto: placeholderImage,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        context.pushNamed(
          AppRouter.reservationDetailsPage,
          extra: repository,
        );
      },
      contentPadding: const EdgeInsets.all(14),
      title: Text(repository.name),
      shape: RoundedRectangleBorder(
        side: const BorderSide(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      subtitle: Text("Last Updated: ${repository.lastUpdatedAt.format}"),
      trailing: Text('${repository.stars} ⭐'),
      leading: Hero(
        tag: 'repo-image',
        child: CircleAvatar(
          backgroundImage: CachedNetworkImageProvider(repository.ownerPhoto),
        ),
      ),
    );
  }
}
