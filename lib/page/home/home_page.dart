import 'package:flutter/material.dart';
import '../../global/helpers/pagination_scroll_controller_helper/pagination_scroll_controller_helper.dart';
import '../../global/widget/loading_widget/loading_widget.dart';
import '../../global/widget/theme_toggle_button.dart';

import '../../global/global.dart';
import '../../global/widget/sort_button.dart';
import 'controller/home_controller.dart';
import 'widget/repo_tile.dart';

class HomePage extends HookConsumerWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(homeControllerProvider);

    final repositories = state.valueOrNull?.data ?? [];

    final scrollController = usePagination(
      () {
        ref.read(homeControllerProvider.notifier).loadMore();
      },
      () => true,
    );

    ref.listen(
      sortControllerProvider,
      (_, next) {
        if (next != null) {
          ref.read(homeControllerProvider.notifier).onSort(next.$1, next.$2);
        } else {
          ref.invalidate(homeControllerProvider);
        }
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text('All Repositories'),
        actions: const [
          SortButton(),
          ThemeToggleButton(),
        ],
      ),
      body: ListView(
        controller: scrollController,
        padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 14),
        shrinkWrap: true,
        physics: const BouncingScrollPhysics(),
        children: [
          ListView.separated(
            padding: const EdgeInsets.only(bottom: 10),
            physics: const ClampingScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              final repo = repositories[index];
              return RepositoryTile(
                key: UniqueKey(),
                repository: repo,
              );
            },
            separatorBuilder: (_, __) => const SizedBox(height: 10),
            itemCount: repositories.length,
          ),
          if (state.isLoading) const LoadingWidget(),
        ],
      ),
    );
  }
}
