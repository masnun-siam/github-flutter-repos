import '../../../global/enums/enums.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../../data/repositories/api_repository_impl/api_repository_impl.dart';
import '../../../domain/entities/paginated_response/paginated_response.dart';
import '../../../domain/entities/pagination_data_request/pagination_data_request.dart';
import '../../../global/mixins/pagination_controller.dart';

part 'home_controller.g.dart';

@Riverpod(keepAlive: true)
class SortController extends _$SortController {
  @override
  (SortedBy, SortingOrder)? build() {
    return null;
  }

  void setSorting(SortedBy sortedBy, SortingOrder sortingOrder) {
    state = (sortedBy, sortingOrder);
  }

  void clearSorting() {
    state = null;
  }
}

@Riverpod(keepAlive: true)
class HomeController extends _$HomeController with PaginationController {
  @override
  FutureOr<PaginatedResponse> build() async {
    final sort = ref.read(sortControllerProvider);
    return loadData(
      PaginationDataRequest(
        sortBy: sort?.$1,
        order: sort?.$2,
      ),
    );
  }

  @override
  FutureOr<PaginatedResponse> loadData([PaginationDataRequest? request]) {
    return ref
        .read(apiRepositoryProvider)
        .getRepositories(request ?? const PaginationDataRequest())
        .then((value) {
      return value.fold((l) => throw l.message, (r) => r);
    });
  }
}
