// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sortControllerHash() => r'6ba302884e0c1185e3dbbc8d90d523a0280585af';

/// See also [SortController].
@ProviderFor(SortController)
final sortControllerProvider =
    NotifierProvider<SortController, (SortedBy, SortingOrder)?>.internal(
  SortController.new,
  name: r'sortControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sortControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SortController = Notifier<(SortedBy, SortingOrder)?>;
String _$homeControllerHash() => r'a05143ac0f448094e24408a783e0cfd94328005d';

/// See also [HomeController].
@ProviderFor(HomeController)
final homeControllerProvider =
    AsyncNotifierProvider<HomeController, PaginatedResponse>.internal(
  HomeController.new,
  name: r'homeControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$homeControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$HomeController = AsyncNotifier<PaginatedResponse>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
