import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart';
import '../../domain/entities/repository/repository_entity.dart';
import '../../global/extensions/date_time_ext.dart';
import '../../global/helpers/url_launcher_helper/url_launcher_helper.dart';

import '../../../global/global.dart';
import 'widget/appbar_flexible_space.dart';

class RepositoryDetailsPage extends ConsumerWidget {
  final Repository repository;

  const RepositoryDetailsPage({super.key, required this.repository});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.sizeOf(context);
    final theme = ref.watch(themeProvider);

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: Navigator.of(context).pop,
            ),
            expandedHeight: size.height * 0.3,
            flexibleSpace: AppbarFlexibleSpace(
              repository: repository,
            ),
            actions: [
              IconButton(
                icon: const Icon(CupertinoIcons.link),
                color: theme.isDarkMode ? Colors.white : Colors.black,
                padding: const EdgeInsets.only(right: 20),
                onPressed: () {
                  UrlLauncherHelper.launchURL(repository.url);
                },
              ),
            ],
          ),
          SliverPadding(
            padding: const EdgeInsets.all(20.0),
            sliver: SliverList(
              delegate: SliverChildListDelegate.fixed([
                Text(
                  repository.description,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(height: 20),
                _TextWidget(label: 'Owner', value: repository.owner),
                const SizedBox(height: 12),
                Row(
                  children: [
                    Expanded(
                      child: _TextWidget(
                        label: 'Stars',
                        value: repository.stars.toString(),
                      ),
                    ),
                    Expanded(
                      child: _TextWidget(
                        label: 'Forks',
                        value: repository.forks.toString(),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 12),
                Row(
                  children: [
                    Expanded(
                      child: _TextWidget(
                        label: 'Open Issues',
                        value: repository.openIssues.toString(),
                      ),
                    ),
                    Expanded(
                      child: _TextWidget(
                        label: 'Language',
                        value: repository.language,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 12),

                // created at
                _TextWidget(
                  label: 'Created At',
                  value: repository.createdAt.format,
                ),
                const SizedBox(height: 12),
                // updated at
                _TextWidget(
                  label: 'Last Updated At',
                  value: repository.lastUpdatedAt.format,
                ),
                const SizedBox(height: 12),
                if (repository.homepage.isSome())
                  GestureDetector(
                    onTap: () {
                      UrlLauncherHelper.launchURL(
                        repository.homepage.getOrElse(() => ''),
                      );
                    },
                    child: _TextWidget(
                      label: 'Homepage',
                      value: repository.homepage.getOrElse(() => ''),
                    ),
                  ),
              ]),
            ),
          ),
        ],
      ),
    );
  }
}

class _TextWidget extends StatelessWidget {
  final String label;

  final String value;
  const _TextWidget({
    required this.label,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
        children: [
          TextSpan(
            text: '$label: ',
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
          TextSpan(
            text: value,
            style: const TextStyle(
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
