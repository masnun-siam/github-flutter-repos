import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../domain/entities/repository/repository_entity.dart';
class AppbarFlexibleSpace extends StatelessWidget {
  const AppbarFlexibleSpace({
    super.key,
    required this.repository,
  });

  final Repository repository;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.sizeOf(context);
    final appbarHeight = size.height * 0.3;
    final colorBoxHeight = size.height * 0.2;
    final reservedTextHeight = size.height * 0.07;
    final freeSpaceHeight = appbarHeight - colorBoxHeight - reservedTextHeight;
    return Stack(
      children: [
        Positioned.fill(
          child: Column(
            children: [
              Ink(
                height: size.height * 0.25,
                width: size.width,
                color: Theme.of(context).primaryColor,
              ),
              Expanded(
                child: Ink(
                  width: size.width,
                  color: Theme.of(context).scaffoldBackgroundColor,
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: colorBoxHeight - (freeSpaceHeight / 2),
          left: 0,
          right: 0,
          bottom: reservedTextHeight,
          child: Align(
            alignment: Alignment.center,
            child: Hero(
              tag: 'repo-image',
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(
                      repository.ownerPhoto,
                    ),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          child: Container(
            height: reservedTextHeight - 10,
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Text(
              repository.fullName,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.titleLarge,
            ),
          ),
        )
      ],
    );
  }
}