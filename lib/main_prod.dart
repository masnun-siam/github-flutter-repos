import 'package:flutter/material.dart';

import '../global/global.dart';
import 'app_root.dart';
import 'global/helpers/shared_preference_helper/shared_preference_helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Preference.load();
  runApp(
    const ProviderScope(
      child: AppRoot(),
    ),
  );
}
