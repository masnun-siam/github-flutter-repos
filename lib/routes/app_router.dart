import '../domain/entities/repository/repository_entity.dart';
import '../global/global.dart';
import '../page/error/error_page.dart';
import '../page/home/home_page.dart';
import '../page/repository_details/repository_details_page.dart';

class AppRouter {
  static const String _homePath = '/';
  static const String _reservationDetailsPath = '/reservationDetails';

  static String get homePage => '/home';
  static String get reservationDetailsPage => 'reservationDetails';

  GoRouter router = GoRouter(
    initialLocation: _homePath,
    // turn off history tracking in the browser for all navigation
    // routerNeglect: true,
    debugLogDiagnostics: true,
    routes: [
      GoRoute(
        name: homePage,
        path: _homePath,
        builder: (context, state) => const HomePage(),
      ),
      GoRoute(
        name: reservationDetailsPage,
        path: _reservationDetailsPath,
        builder: (context, state) => RepositoryDetailsPage(
          repository: state.extra as Repository,
        ),
      ),
    ],
    errorBuilder: (context, state) => ErrorPage(state.error!),
  );
}
