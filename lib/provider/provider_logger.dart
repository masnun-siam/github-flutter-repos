import 'package:flutter/foundation.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class Logger extends ProviderObserver {
  @override
  void didUpdateProvider(
    ProviderBase provider,
    Object? previousValue,
    Object? newValue,
    ProviderContainer container,
  ) {
    if (kDebugMode) {
      print('👀 [${provider.name ?? provider.runtimeType}] '
          'changed from 👉 $previousValue to 👉 $newValue 👈');
    }
  }
}
