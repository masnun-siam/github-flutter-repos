enum SortedBy {
  stars,
  updated,
}

enum SortingOrder {
  asc,
  desc,
}