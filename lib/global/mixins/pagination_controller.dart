// ignore_for_file: invalid_use_of_internal_member
// ignore_for_file: implementation_imports

import 'dart:async';

import 'package:github_flutter_repos/global/enums/enums.dart';
import 'package:riverpod/src/async_notifier.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../domain/entities/paginated_response/paginated_response.dart';
import '../../domain/entities/pagination_data_request/pagination_data_request.dart';

mixin PaginationController on AsyncNotifierBase<PaginatedResponse> {
  bool canLoadMore() {
    if (state.isLoading) return false;
    if (!state.hasValue) return false;
    if (!state.requireValue.canFetchMore) return false;
    return true;
  }

  FutureOr<PaginatedResponse> loadData([PaginationDataRequest? request]);

  Future<void> loadMore([PaginationDataRequest? request]) async {
    final oldState = state;
    if (!oldState.requireValue.canFetchMore || state.isLoading) return;
    state = const AsyncLoading<PaginatedResponse>().copyWithPrevious(oldState);
    state = await AsyncValue.guard<PaginatedResponse>(() async {
      final res = await loadData(oldState.requireValue.nextPage(request));
      final newRes = res.copyWith(data: [
        ...state.requireValue.data,
        if (res.currentPage != 1) ...res.data,
      ]);
      return newRes;
    });
  }

  void onSort(SortedBy sort, [SortingOrder order = SortingOrder.desc]) async {
    state = AsyncData(
      PaginatedResponse(
        currentPage: 1,
        perPage: 10,
        canFetchMore: false,
        data: [],
      ),
    );
    state = const AsyncLoading<PaginatedResponse>().copyWithPrevious(state);
    state = await AsyncValue.guard<PaginatedResponse>(() async {
      return loadData(
        PaginationDataRequest(
          sortBy: sort,
          order: order,
        ),
      );
    });
  }
}
