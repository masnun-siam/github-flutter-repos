import 'package:intl/intl.dart';

extension DateTimeExt on DateTime {
  String get format => DateFormat('MM-dd-yy HH:ss').format(this);
}