import 'package:dio/dio.dart';
import '../../../domain/failures/failures.dart';
import '../../global.dart';

Failures handleDioExceptions(DioException e) {
  switch (e.type) {
    case DioExceptionType.connectionTimeout:
      return const Failures.timeout();
    case DioExceptionType.sendTimeout:
      return const Failures.timeout();
    case DioExceptionType.receiveTimeout:
      return const Failures.timeout();
    case DioExceptionType.badResponse:
      return _response(e, 'Bad response');
    case DioExceptionType.cancel:
      return const Failures.response('Request cancelled');
    case DioExceptionType.unknown:
      return _response(e, 'Unknown error');
    case DioExceptionType.badCertificate:
      return const Failures.response('Bad certificate');
    case DioExceptionType.connectionError:
      return const Failures.network();
  }
}

Failures _response(DioException e, String message) {
  if (e.response?.data != null) {
    return Failures.response(
        (e.response!.data as JsonMap?)?['message']?.toString() ?? message);
  }
  return Failures.response(message);
}
