import 'package:github_flutter_repos/global/helpers/shared_preference_helper/shared_preference_helper.dart';

bool cacheTimeoutChecker() {
  final cacheDateTime = Preference.getString('cacheDateTime');
  if (cacheDateTime.isEmpty) {
    return false;
  }
  final cacheTime = DateTime.parse(cacheDateTime);
  final currentTime = DateTime.now();
  final difference = currentTime.difference(cacheTime).inMinutes;
  return difference < 30;
}
