import 'package:shared_preferences/shared_preferences.dart';

class Preference {
  static SharedPreferences? _preference;

  static Future<SharedPreferences> load() async {
    _preference ??= await SharedPreferences.getInstance();
    return _preference!;
  }

  static void setString(String key, String value) {
    _preference!.setString(key, value);
  }

  static void setInt(String key, int value) {
    _preference!.setInt(key, value);
  }

  static void setDouble(String key, double value) {
    _preference!.setDouble(key, value);
  }

  static void setBool(String key, bool value) {
    _preference!.setBool(key, value);
  }

  static String getString(String key, {String def = ''}) {
    String? val;
  
    val ??= _preference!.getString(key);
    val ??= def;
    return val;
  }

  static int getInt(String key, {int def = 0}) {
    int? val;
    val ??= _preference!.getInt(key);
    val ??= def;
    return val;
  }

  static double getDouble(String key, {double def = 0}) {
    double? val;
    val ??= _preference!.getDouble(key);
    val ??= def;
    return val;
  }

  static bool getBool(String key, {bool def = false}) {
    bool? val;
    val ??= _preference!.getBool(key);
    val ??= def;
    return val;
  }
}