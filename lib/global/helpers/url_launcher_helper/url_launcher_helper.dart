import 'package:url_launcher/url_launcher_string.dart';

class UrlLauncherHelper {
  static Future<void> launchURL(String url) async {
      await launchUrlString(url, mode: LaunchMode.inAppBrowserView);
  }
}