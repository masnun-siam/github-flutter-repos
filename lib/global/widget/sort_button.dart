import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart';
import '../enums/enums.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../page/home/controller/home_controller.dart';
import '../../page/home/widget/sort_bottom_sheet.dart';

class SortButton extends ConsumerWidget {
  const SortButton({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final sort = ref.watch(sortControllerProvider);
    return IconButton(
      icon: Icon(
        optionOf(sort).fold(
          () => CupertinoIcons.sort_down,
          (t) => switch (t.$2) {
            SortingOrder.asc => CupertinoIcons.sort_up_circle_fill,
            SortingOrder.desc => CupertinoIcons.sort_down_circle_fill,
          },
        ),
      ),
      onPressed: () {
        showModalBottomSheet(
          context: context,
          showDragHandle: true,
          enableDrag: true,
          isDismissible: true,
          isScrollControlled: true,
          builder: (context) {
            return const SortBottomSheet();
          },
        );
      },
    );
  }
}
