import 'package:flutter/material.dart';
import '../../../page/home/widget/repo_tile.dart';
import 'package:skeletonizer/skeletonizer.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Skeletonizer(
      enabled: true,
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) => RepositoryTile.loading(),
        separatorBuilder: (context, index) => const SizedBox(height: 10),
        itemCount: 4,
      ),
    );
  }
}
