import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter/material.dart';

import '../theme/theme.dart';

class ThemeToggleButton extends HookConsumerWidget {
  const ThemeToggleButton({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final themeData = ref.watch(themeProvider);
    return IconButton(
      icon: Icon(
        themeData.isDarkMode ? Icons.light_mode : Icons.dark_mode,
      ),
      onPressed: ref.read(themeProvider).toggleTheme,
    );
  }
}
