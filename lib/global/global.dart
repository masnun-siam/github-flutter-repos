// Packages
export 'package:flutter/foundation.dart' show debugPrint;
export 'package:hooks_riverpod/hooks_riverpod.dart';
export 'package:google_fonts/google_fonts.dart';
export 'package:go_router/go_router.dart';

export '../routes/app_router.dart';
export '../provider/provider_logger.dart';
export '../routes/router_provider.dart';
export '../global/theme/theme.dart';

typedef JsonMap = Map<String, dynamic>;
