# Github Flutter Repos

## Description

A Flutter app that shows all the github repositories from the github search result 'Flutter'. It also allows user to sort the results by stars and updates.

## Installation

Get all the required packages by running this command in the project root folder.

```sh
flutter pub get
```

Then run the project by running this command

```sh
flutter run
```

# Main Packages and Plugins

- **State Management:** riverpod
- **Local Storage:** shared_preferences
- **Http Request Handler:** dio

# Project Structure

This project is created following the **'Riverpod Architecture'** which is proposed by **Andrea Bizzotto**. This is the overview of the project folder structure.

```sh
.
├── app_root.dart
├── data
│   ├── core
│   │   └── freezed_key
│   │       └── freezed_key.dart
│   ├── model
│   │   ├── paginated_response_model
│   │   │   ├── paginated_response_model.dart
│   │   │   ├── paginated_response_model.freezed.dart
│   │   │   └── paginated_response_model.g.dart
│   │   ├── pagination_request_model
│   │   │   ├── pagination_request_model.dart
│   │   │   ├── pagination_request_model.freezed.dart
│   │   │   └── pagination_request_model.g.dart
│   │   ├── repository_model
│   │   │   ├── repository_model.dart
│   │   │   ├── repository_model.freezed.dart
│   │   │   └── repository_model.g.dart
│   │   └── repository_owner_model
│   │       ├── repository_owner_model.dart
│   │       ├── repository_owner_model.freezed.dart
│   │       └── repository_owner_model.g.dart
│   ├── repositories
│   │   └── api_repository_impl
│   │       ├── api_repository_impl.dart
│   │       ├── api_repository_impl.g.dart
│   │       └── fake_api_repository.dart
│   └── source
│       └── remote
│           ├── cache_interceptor.dart
│           ├── http_client.dart
│           ├── http_client.g.dart
│           └── token_interceptor.dart
├── domain
│   ├── entities
│   │   ├── paginated_response
│   │   │   ├── paginated_response.dart
│   │   │   └── paginated_response.freezed.dart
│   │   ├── pagination_data_request
│   │   │   ├── pagination_data_request.dart
│   │   │   └── pagination_data_request.freezed.dart
│   │   └── repository
│   │       ├── repository_entity.dart
│   │       └── repository_entity.freezed.dart
│   ├── failures
│   │   ├── failures.dart
│   │   └── failures.freezed.dart
│   └── repositories
│       └── api_repository
│           └── api_repository.dart
├── global
│   ├── constants
│   │   └── strings.dart
│   ├── enums
│   │   └── enums.dart
│   ├── extensions
│   │   └── date_time_ext.dart
│   ├── global.dart
│   ├── helpers
│   │   ├── dio_helpers
│   │   │   └── handle_dio_exception.dart
│   │   ├── pagination_scroll_controller_helper
│   │   │   └── pagination_scroll_controller_helper.dart
│   │   ├── shared_preference_helper
│   │   │   └── shared_preference_helper.dart
│   │   └── url_launcher_helper
│   │       └── url_launcher_helper.dart
│   ├── mixins
│   │   └── pagination_controller.dart
│   ├── theme
│   │   ├── custom_theme_data.dart
│   │   ├── dark_theme.dart
│   │   ├── light_theme.dart
│   │   ├── theme.dart
│   │   └── theme_service.dart
│   └── widget
│       ├── loading_widget
│       │   └── loading_widget.dart
│       ├── sort_button.dart
│       ├── theme_toggle_button.dart
│       └── widget.dart
├── main_dev.dart
├── main_prod.dart
├── page
│   ├── error
│   │   └── error_page.dart
│   ├── home
│   │   ├── controller
│   │   │   ├── home_controller.dart
│   │   │   └── home_controller.g.dart
│   │   ├── home_page.dart
│   │   └── widget
│   │       ├── repo_tile.dart
│   │       └── sort_bottom_sheet.dart
│   └── repository_details
│       ├── controller
│       │   └── repository_details_controller.dart
│       ├── repository_details_page.dart
│       └── widget
│           └── appbar_flexible_space.dart
├── provider
│   └── provider_logger.dart
└── routes
    ├── app_router.dart
    └── router_provider.dart
```

# Challenges

These were the requirements for the specific project.

- [x] Fetch repository list from GitHub API using "Flutter" as query keyword.
- [x] The fetched data should be stored in a local database to permit the app to be used in offline mode.
- [x] Fetching the repository list should be paginated by scrolling. Each time by scrolling, fetch 10 new items.
- [x] The required data can be refreshed from the API no more frequently than once every 30 minutes.
- [x] Show the list of repositories on the home page.
- [x] List can be sorted by either the last updated date-time or star count (add a sorting button/icon)
- [x] Selected sorting option persists in further app sessions.
- [x] A repo details page, which is navigated by clicking on an item from the list.
- [x] Details page shows repo owner's name, photo, repository's description, last update date time in month-day-year hour:seconds format, each field in 2 digit numbers and any other fields you want
- [x] The repository list and repository details data that loaded once, should be saved for offline browsing.

This is how I have implemented the requirements

## Fetching Repository List from Github Search API using the query 'Flutter'

I have made an http request in 'https://api.github.com/search/repositories?q=Flutter&page=1&per_page=10' this url and showed the repository list from the response in the home page.

## The fetched data should be stored in a local database to permit the app to be used in offline mode

For Handling the issue, I first started working with Hive. But later I switched to a different solution. Now the app is caching the response in local database by using a **Cache Interceptor** in the dio plugin that is storing the response with the full url as key.

## Fetching the repository list should be paginated by scrolling. Each time by scrolling, fetch 10 new items

Solved this challenge by adding a listener in the ListView's ScrollController to fetch more data when the scroll position is greater or equal than the maxScrollExtent.

<video width="400" controls>
  <source src="./resources/pagination.mp4" type="video/mp4">
</video>

## The required data can be refreshed from the API no more frequently than once every 30 minutes

Solved this challenge by storing the first time caching time in local storage and allowing the http request to fetch data from remote instead of local data and updating the cache time if the last cached time difference from now is greater or equal to 30 minutes.

## Show the list of repositories on the home page

The repositories are shown in home page using ListView.builder to prevent unnecessary builds and improve performance.

| Light Mode                             | Dark Mode                             |
| -------------------------------------- | ------------------------------------- |
| ![Image 1](./resources/home_light.png) | ![Image 2](./resources/home_dark.png) |

## List can be sorted by either the last updated date-time or star count (add a sorting button/icon)

Solved this challenge by adding a sorting button in the appbar and added a modal bottom sheet with available sorting options to sort the repository list accordingly.

<video width="400" controls>
  <source src="./resources/sorting.mp4" type="video/mp4">
</video>

## Selected sorting option persists in further app sessions

Solved this challenge by disabling the sorting controller's auto dispose feature.

<video width="400" controls>
  <source src="./resources/sorting_state_session.mp4" type="video/mp4">
</video>

## A repo details page, which is navigated by clicking on an item from the list

Solved this challenge by adding a details page and passing the repository data to that page.

<video width="400" controls>
  <source src="./resources/details_navigation.mp4" type="video/mp4">
</video>

## Details page shows repo owner's name, photo, repository's description, last update date time in month-day-year hour:seconds format, each field in 2 digit numbers 

Added the required sections with specific formats in the details page.

<img width="400" src="./resources/details.png">
</img>

## The repository list and repository details data that loaded once, should be saved for offline browsing

The responses are cached to local storage using the **shared_preferences** plugin to solve this challenge.

# Additional Information

- The app has Day/Night theme support.
- The project uses **riverpod** as a state management solution. Which is a widely popular and Flutter Favorite state management solution.
- The project has **Unit Test** Coverage.
- The project has Repository pattern with an appropriate abstraction layer.
- The project has Central APl call error handling.
- The project has different flavor support (DEV and PROD).
- The project has Token Interceptor added for handling auth tokens with APl calls in the future.