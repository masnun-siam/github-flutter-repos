import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fpdart/fpdart.dart';
import 'package:github_flutter_repos/data/repositories/api_repository_impl/api_repository_impl.dart';
import 'package:github_flutter_repos/data/source/remote/http_client.dart';
import 'package:github_flutter_repos/domain/entities/paginated_response/paginated_response.dart';
import 'package:github_flutter_repos/domain/entities/pagination_data_request/pagination_data_request.dart';
import 'package:github_flutter_repos/domain/failures/failures.dart';
import 'package:github_flutter_repos/domain/repositories/api_repository/api_repository.dart';
import 'package:github_flutter_repos/global/enums/enums.dart';
import 'package:github_flutter_repos/global/global.dart';
import 'package:mocktail/mocktail.dart';

void main() {
  final container = ProviderContainer(overrides: [
    httpClientProvider.overrideWithValue(MockHttpClient()),
  ]);

  group('Api Repository Test -', () {
    late ApiRepository apiRepository;
    late PaginationDataRequest paginationDataRequest;
    setUp(() {
      apiRepository = container.read(apiRepositoryProvider);
      paginationDataRequest = const PaginationDataRequest(
        page: 1,
        perPage: 10,
        sortBy: SortedBy.stars,
        order: SortingOrder.desc,
      );
    });
    test('should get a pagination model response', () async {
      // arrange
      when(() => container.read<HttpClient>(httpClientProvider).get(any(),
          queryParameters: any(named: 'queryParameters'),
          addToken: any(named: 'addToken'))).thenAnswer((_) async {
        final jsonStr = File('json/repo_list.json').readAsStringSync();
        return jsonDecode(jsonStr);
      });
      // act
      final result = await apiRepository.getRepositories(paginationDataRequest);

      // assert
      expect(result.isRight(), true);
    });

    test('should get a Failure.response() on return', () async {
      // arrange
      when(
        () => container.read<HttpClient>(httpClientProvider).get(any(),
            queryParameters: any(named: 'queryParameters'),
            addToken: any(named: 'addToken')),
      ).thenThrow(DioException(
        response: Response(
          statusCode: 404,
          requestOptions: RequestOptions(path: ''),
          data: {
            'message': 'Not Found',
            'success': false,
            'data': null,
          },
        ),
        requestOptions: RequestOptions(path: ''),
      ));

      // act
      final result = await apiRepository.getRepositories(paginationDataRequest);

      // assert
      expect(
          result,
          const Left<Failures, PaginatedResponse>(
              Failures.response('Not Found')));
    });
  });
}

class MockHttpClient extends Mock implements HttpClient {}
