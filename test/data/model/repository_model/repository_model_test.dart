import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:github_flutter_repos/data/model/repository_model/repository_model.dart';

void main() {
  group('RepositoryModel', () {
    RepositoryModel? repositoryObject;

    setUp(() {
      final jsonString = File('json/single_repo.json').readAsStringSync();
      final jsonMap = jsonDecode(jsonString);
      try {
        repositoryObject = RepositoryModel.fromJson(jsonMap);
      } catch (err) {
        log(err.toString());
      }
    });

    test('should be a type of RepositoryModel', () async {
      // assert
      expect(repositoryObject, isA<RepositoryModel>());
    });

    test('should be the owner id of 14101776', () {
      // assert
      expect(repositoryObject?.owner?.id, 14101776);
    });
  });
}
